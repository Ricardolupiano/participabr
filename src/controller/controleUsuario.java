package controller;

import java.util.ArrayList;

import model.usuario;

public class controleUsuario {
	private ArrayList<usuario> umaLista;
	
	public controleUsuario(){
		
		umaLista = new ArrayList<usuario>();
	}

	public ArrayList<usuario> getUmaLista() {
		return umaLista;
	}

	public void setUmaLista(ArrayList<usuario> umaLista) {
		this.umaLista = umaLista;
	}
	public void adicionarUsuario(usuario umUsuario){
		umaLista.add(umUsuario);
	}
	public void removerUsuario(usuario umUsuario){
		umaLista.remove(umUsuario);
	}
	public usuario pesquisar(String nome){
		for (usuario u:umaLista)
			if(u.getNome().equalsIgnoreCase(nome))
				return u;
		return null;
	}
	
	
}
