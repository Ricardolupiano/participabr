package controller;
import java.util.ArrayList;

import model.post;

public class controlePost {
	private ArrayList<post> umaListaPosts;
	
	public controlePost (){
		umaListaPosts=new ArrayList<post>();
	}
	public ArrayList<post> getUmaListaPosts() {
		return umaListaPosts;
	}
	public void setUmaListaPosts(ArrayList<post> umaListaPosts) {
		this.umaListaPosts = umaListaPosts;
	}
	public void adicionarPost(post umPost){
		umaListaPosts.add(umPost);
	}
	public void removePost(post umPost){
		umaListaPosts.remove(umPost);
	}
	public post pesquisarPost(String assunto){
		for (post p:umaListaPosts)
			if(p.getAssunto().equalsIgnoreCase(assunto))
				return p;
		return null;
	}
}
