package test;

import static org.junit.Assert.*;
import model.Endereco;
import model.elogio;
import model.post;
import model.usuario;

import org.junit.Before;
import org.junit.Test;

public class testElogio {
	private static Endereco umEndereco;
	private static usuario umUsuario;
	private static post umPost;
	private static elogio umComentario;
	@Before
	public void setUp() throws Exception{
	umEndereco = new Endereco("BR","estado","cidade", "cep");
	umUsuario = new usuario("nome","usuario","senha","email",umEndereco);
	umPost = new post("assunto",umUsuario);
	umComentario = new elogio("comentario","assunto",umUsuario, umPost);
	}
	@Test
	public void testComentario() {
		assertEquals("comentario", umComentario.getComentario());
	}
	@Test
	public void testeAssunto(){
		assertEquals("assunto", umComentario.getAssunto());
	}
	@Test
	public void testeUsuario(){
		assertEquals(umUsuario, umComentario.getUmUsuario());
	}
	@Test
	public void testePost(){
		assertEquals(umPost, umComentario.getUmPost());
	}
}
