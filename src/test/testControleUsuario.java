package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Endereco;
import model.usuario;

import org.junit.Before;
import org.junit.Test;

import controller.controleUsuario;

public class testControleUsuario {
	private static Endereco umEndereco;
	private static usuario umUsuario;
	private static controleUsuario umControle;
	private static ArrayList<usuario> lista;
	@Before
	public void setUp() throws Exception{
	umEndereco = new Endereco("BR","estado","cidade", "cep");
	umUsuario = new usuario("nome","usuario","senha","email",umEndereco);
	umControle = new controleUsuario();
	lista = new ArrayList<usuario>();
	}
	
	@Test
	public void testSeteGet() {
		umControle.setUmaLista(lista);
		assertEquals(lista, umControle.getUmaLista());	
	}

	@Test
	public void testAdicionareRemover() {
		umControle.adicionarUsuario(umUsuario);
		umControle.adicionarUsuario(umUsuario);
		assertEquals(2,umControle.getUmaLista().size());
		umControle.removerUsuario(umUsuario);
		assertEquals(1,umControle.getUmaLista().size());
		
	}
	@Test
	public void testPesquisar(){
		umControle.adicionarUsuario(umUsuario);
		assertEquals(umUsuario, umControle.pesquisar("nome"));
	}
	
}
