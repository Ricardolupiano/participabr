package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import model.Endereco;
import model.post;
import model.usuario;
import org.junit.Before;
import org.junit.Test;
import controller.controlePost;

public class testControlePost {

	private static Endereco umEndereco;
	private static usuario umUsuario;
	private static post umPost;
	private static controlePost umControle;
	private static ArrayList<post> lista;
	
	@Before
	public void setUp() throws Exception{
	umEndereco = new Endereco("BR","estado","cidade", "cep");
	umUsuario = new usuario("nome","usuario","senha","email",umEndereco);
	umPost = new post("assunto",umUsuario);
	umControle = new controlePost();
	lista = new ArrayList<post>();
	}
	
	@Test
	public void testSeteGet() {
		umControle.setUmaListaPosts(lista);
		assertEquals(lista, umControle.getUmaListaPosts());
	}

	@Test
	public void testAdicionar() {
		umControle.adicionarPost(umPost);
		umControle.adicionarPost(umPost);
		assertEquals(2,umControle.getUmaListaPosts().size());
		umControle.removePost(umPost);
		assertEquals(1,umControle.getUmaListaPosts().size());	
	}
	
	@Test
	public void pesquisar(){
		umControle.adicionarPost(umPost);
		assertEquals(umPost, umControle.pesquisarPost("assunto"));
	}
}
