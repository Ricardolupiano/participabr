package test;

import static org.junit.Assert.*;
import model.comentario;
import model.Endereco;
import model.post;
import model.usuario;
import org.junit.Before;
import org.junit.Test;

public class testComentario {
	private static Endereco umEndereco;
	private static usuario umUsuario;
	private static post umPost;
	private static comentario umComentario;
	@Before
	public void setUp() throws Exception{
	umEndereco = new Endereco("BR","estado","cidade", "cep");
	umUsuario = new usuario("nome","usuario","senha","email",umEndereco);
	umPost = new post("assunto",umUsuario);
	umComentario = new comentario("comentario","assunto",umUsuario, umPost);
	}
	@Test
	public void testeComentario() {
		assertEquals("comentario", umComentario.getComentario());
	}
	@Test
	public void testeAssunto(){
		assertEquals("assunto", umComentario.getAssunto());
	}
	@Test
	public void testeUsuario(){
		assertEquals(umUsuario, umComentario.getUmUsuario());
	}
	@Test
	public void testePost(){
		assertEquals(umPost, umComentario.getUmPost());
	}
}
