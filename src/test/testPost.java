package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Endereco;
import model.comentario;
import model.post;
import model.usuario;

import org.junit.Before;
import org.junit.Test;

public class testPost {
	private static Endereco umEndereco;
	private static usuario umUsuario;
	private static post umPost;
	private static comentario umComentario;
	private static ArrayList<comentario> lista = new ArrayList<comentario>();
	
	@Before
	public void setUp() throws Exception{
	umEndereco = new Endereco("BR","estado","cidade", "cep");
	umUsuario = new usuario("nome","usuario","senha","email",umEndereco);
	umPost = new post("assunto",umUsuario);
	umComentario = new comentario("comentario","assunto",umUsuario, umPost);
	lista = new ArrayList<comentario>();
	}
	
	@Test
	public void testAssunto() {
		assertEquals("assunto", umPost.getAssunto());
	}
	@Test
	public void testUsuario(){
		assertEquals(umUsuario, umPost.getUmUsuario());
	}
	@Test
	public void testLista() {
		umPost.setUmaListaComentarios(lista);
		assertEquals(lista, umPost.getUmaListaComentarios());
	}
	@Test
	public void testAdicionarRemoverLista() {
		umPost.adicionarComentario(umComentario);
		umPost.adicionarComentario(umComentario);
		assertEquals(2,umPost.getUmaListaComentarios().size());
		umPost.removeComentario(umComentario);
		assertEquals(1,umPost.getUmaListaComentarios().size());
		
	}
	
}
