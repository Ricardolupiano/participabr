package test;

import static org.junit.Assert.*;
import model.Endereco;
import model.usuario;
import org.junit.Before;
import org.junit.Test;

public class testeUsuario {
	private static Endereco umEndereco;
	private static usuario umUsuario;
	
	@Before
	public void setUp() throws Exception{
	umEndereco = new Endereco("BR","estado","cidade", "cep");
	umUsuario = new usuario("nome","login","senha","email",umEndereco);
	}
	@Test
	public void testNome(){
		assertEquals("nome",umUsuario.getNome());
	}
	@Test
	public void testLogin(){
		assertEquals("login",umUsuario.getUsuario());
	}
	@Test
	public void testSenha(){
		assertEquals("senha",umUsuario.getSenha());
	}
	@Test
	public void testEmail(){
		assertEquals("email",umUsuario.getEmail());
	}
	@Test
	public void testEndereco(){
		assertEquals("BR",umUsuario.getEndereco().getPais());
	}
}
