package test;

import static org.junit.Assert.*;
import model.Endereco;
import model.critica;
import model.post;
import model.usuario;

import org.junit.Before;
import org.junit.Test;

public class testCritica {

	private static Endereco umEndereco;
	private static usuario umUsuario;
	private static post umPost;
	private static critica umComentario;
	@Before
	public void setUp() throws Exception{
	umEndereco = new Endereco("BR","estado","cidade", "cep");
	umUsuario = new usuario("nome","usuario","senha","email",umEndereco);
	umPost = new post("assunto",umUsuario);
	umComentario = new critica("comentario","assunto",umUsuario, umPost);
	}
	@Test
	public void testComentario() {
		assertEquals("comentario", umComentario.getComentario());
	}
	@Test
	public void testAssunto(){
		assertEquals("assunto", umComentario.getAssunto());
	}
	@Test
	public void testUsuario(){
		assertEquals(umUsuario, umComentario.getUmUsuario());
	}
	@Test
	public void testPost(){
		assertEquals(umPost, umComentario.getUmPost());
	}

}
