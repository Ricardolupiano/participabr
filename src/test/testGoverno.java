package test;

import static org.junit.Assert.*;
import model.Endereco;
import model.governo;
import org.junit.Before;
import org.junit.Test;

public class testGoverno {
	private static Endereco umEndereco;
	private static governo umFuncionario;
	@Before
	public void setUp() throws Exception{
	umEndereco = new Endereco("BR","estado","cidade", "cep");
	umFuncionario = new governo("cargo","matricula","nome","usuario","senha","email",umEndereco);
	}
	@Test
	public void testeCargo() {
		assertEquals("cargo", umFuncionario.getCargo());
		
	}
	@Test
	public void testeMatricula() {
		assertEquals("matricula", umFuncionario.getMatricula());
	}
	@Test
	public void testeNome(){
		assertEquals("nome", umFuncionario.getNome());
	}
	@Test
	public void testeUsuario(){
		assertEquals("usuario", umFuncionario.getUsuario());
	}
	@Test
	public void testeSenha(){
		assertEquals("senha", umFuncionario.getSenha());
	}
	@Test
	public void testeEmail(){
		assertEquals("email", umFuncionario.getEmail());
	}
	@Test
	public void testeEndereco(){
		assertEquals(umEndereco, umFuncionario.getEndereco());
	}


}
