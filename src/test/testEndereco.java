package test;

import static org.junit.Assert.*;
import model.Endereco;

import org.junit.Before;
import org.junit.Test;

public class testEndereco {
	private static Endereco umEndereco;
	
	@Before
	public void setUp() throws Exception{
	umEndereco = new Endereco("BR","estado","cidade", "cep");
	}
	
	@Test
	public void testePais() {
		assertEquals("BR",umEndereco.getPais());
	}
	@Test
	public void testeEstado() {
		assertEquals("estado",umEndereco.getEstado());
	}
	@Test
	public void testeCidade() {
		assertEquals("cidade",umEndereco.getCidade());
	}
	@Test
	public void testCep() {
		assertEquals("cep",umEndereco.getCep());
	}
}
