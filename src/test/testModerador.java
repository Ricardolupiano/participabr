package test;

import static org.junit.Assert.*;
import model.Endereco;
import model.moderador;

import org.junit.Before;
import org.junit.Test;

public class testModerador {

	private static Endereco umEndereco;
	private static moderador umFuncionario;
	@Before
	public void setUp() throws Exception{
	umEndereco = new Endereco("BR","estado","cidade", "cep");
	umFuncionario = new moderador("nivel","nome","usuario","senha","email",umEndereco);
	}
	@Test
	public void testeNivel() {
		assertEquals("nivel", umFuncionario.getNivelAcesso());
	}
	@Test
	public void testeNome() {
		assertEquals("nome", umFuncionario.getNome());
	}
	@Test
	public void testeUsuario() {
		assertEquals("usuario", umFuncionario.getUsuario());
	}
	@Test
	public void testeSenha() {
		assertEquals("senha", umFuncionario.getSenha());
	}
	@Test
	public void testeEmail() {
		assertEquals("email", umFuncionario.getEmail());
	}
	@Test
	public void testeEndereco() {
		assertEquals("BR", umFuncionario.getEndereco().getPais());
	}

}
