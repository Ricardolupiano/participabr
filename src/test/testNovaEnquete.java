package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import model.novaEnquete;
import model.Endereco;
import model.comentario;
import model.usuario;

import org.junit.Before;
import org.junit.Test;

public class testNovaEnquete {

	private static Endereco umEndereco;
	private static usuario umUsuario;
	private static novaEnquete umPost;
	private static comentario umComentario;
	private static ArrayList<comentario> lista = new ArrayList<comentario>();
	@Before
	public void setUp() throws Exception{
	umEndereco = new Endereco("BR","estado","cidade", "cep");
	umUsuario = new usuario("nome","usuario","senha","email",umEndereco);
	umPost = new novaEnquete("assunto",umUsuario);
	umComentario = new comentario("comentario","assunto",umUsuario, umPost);
	lista = new ArrayList<comentario>();
	}
	@Test
	public void testVotos() {
		assertEquals("assunto", umPost.getAssunto());
		assertEquals("nome", umPost.getUmUsuario().getNome());
		assertEquals(0, umPost.getVotoTipo1());
		umPost.votar1();
		assertEquals(1, umPost.getVotoTipo1());
		assertEquals(0, umPost.getVotoTipo2());
		umPost.votar2();
		assertEquals(1, umPost.getVotoTipo2());
		
	}
	@Test
	public void testLista() {
		umPost.setUmaListaComentarios(lista);
		assertEquals(lista, umPost.getUmaListaComentarios());
		
	}
	@Test
	public void testAdicionarLista() {
		umPost.adicionarComentario(umComentario);
		umPost.adicionarComentario(umComentario);
		assertEquals(2,umPost.getUmaListaComentarios().size());
		umPost.removeComentario(umComentario);
		assertEquals(1,umPost.getUmaListaComentarios().size());
		
	}

}
