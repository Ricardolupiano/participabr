package model;

public class Endereco {
	private String pais;
	private String estado;
	private String cidade;
	private String cep;
	
	public Endereco(String pais,String estado,String cidade, String cep){
		this.pais=pais;
		this.estado=estado;
		this.cidade=cidade;
		this.cep=cep;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	
}
