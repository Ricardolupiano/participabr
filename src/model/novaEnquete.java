package model;

public class novaEnquete extends post {
	private int votoTipo1=0;
	private int votoTipo2=0;
	
	public novaEnquete(String assunto, usuario umUsuario) {
		super(assunto, umUsuario);
	}

	public int getVotoTipo1() {
		return votoTipo1;
	}

	public void setVotoTipo1(int votoTipo1) {
		this.votoTipo1 = votoTipo1;
	}

	public int getVotoTipo2() {
		return votoTipo2;
	}

	public void setVotoTipo2(int votoTipo2) {
		this.votoTipo2 = votoTipo2;
	}
	
	public void votar1(){
		votoTipo1 = votoTipo1+1;
	}
	public void votar2(){
		this.votoTipo2+=1;
	}

}
