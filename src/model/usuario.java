package model;

public class usuario {
	protected String nome;
	protected String usuario;
	protected String senha;
	protected String email;
	private Endereco endereco;
	
	public usuario(String nome,String usuario, String senha,String email,Endereco endereco){
		this.nome=nome;
		this.usuario=usuario;
		this.senha=senha;
		this.email=email;
		this.endereco=endereco;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	
}
