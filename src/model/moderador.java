package model;

public class moderador extends usuario {
	private String nivelAcesso;
	
	public moderador(String nivel, String nome, String usuario,String senha,String email,Endereco endereco){
		super(nome,usuario,senha,email,endereco);
		this.nivelAcesso=nivel;
	}

	public String getNivelAcesso() {
		return nivelAcesso;
	}

	public void setNivelAcesso(String nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}
	
	
}
