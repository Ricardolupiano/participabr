package model;

public class novoPost extends post {
	private String comentario;
	
	public novoPost(String assunto, usuario umUsuario, String comentario){
		super(assunto,umUsuario);
		this.comentario=comentario;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
}
