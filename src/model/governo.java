package model;

public class governo extends usuario {
	private String cargo;
	private String matricula;
	
	public governo(String cargo,String matricula, String nome, String usuario,String senha,String email,Endereco endereco){
		super(nome,usuario,senha,email,endereco);
		this.cargo=cargo;
		this.matricula=matricula;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	

}
