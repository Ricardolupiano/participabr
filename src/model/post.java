package model;

import java.util.ArrayList;

public class post {
	protected String assunto;
	protected usuario umUsuario;
	protected ArrayList<comentario> umaListaComentarios = new ArrayList<comentario>();
	
	public post(String assunto, usuario umUsuario){
		this.assunto=assunto;
		this.umUsuario=umUsuario;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public usuario getUmUsuario() {
		return umUsuario;
	}

	public void setUmUsuario(usuario umUsuario) {
		this.umUsuario = umUsuario;
	}
	
	public void adicionarComentario(comentario umComentario){
		this.umaListaComentarios.add(umComentario);
	}
	
	public void removeComentario(comentario umComentario){
		this.umaListaComentarios.remove(umComentario);
	}
	public comentario pesquisaComentario(String assunto){
		for (comentario c :umaListaComentarios)
			if(c.getAssunto().equalsIgnoreCase(assunto))
				return c;
		return null;
	}
	public ArrayList<comentario> getUmaListaComentarios() {
		return umaListaComentarios;
	}

	public void setUmaListaComentarios(ArrayList<comentario> umaListaComentarios) {
		this.umaListaComentarios = umaListaComentarios;
	}
	
	
	
	
}
