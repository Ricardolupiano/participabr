package model;

public class comentario {
	private String assunto;
	private String comentario;
	private usuario umUsuario;
	private post umPost;
	
	public comentario(String comentario, String assunto, usuario umUsuario,post umPost){
		this.comentario=comentario;
		this.assunto=assunto;
		this.umUsuario=umUsuario;
		this.umPost=umPost;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public usuario getUmUsuario() {
		return umUsuario;
	}

	public void setUmUsuario(usuario umUsuario) {
		this.umUsuario = umUsuario;
	}

	public post getUmPost() {
		return umPost;
	}

	public void setUmPost(post umPost) {
		this.umPost = umPost;
	}
	
}
